import tensorflow as tf

from dnnmodel import model as mdl
import parameters as param
from datamanipulation import test_images_loading as til
import os

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)


x_test, y_test = til.load_test_images()
test_images_generator = til.get_test_generator(x_test, y_test)

path_to_models = os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models')
model_names = sorted(os.listdir(path_to_models))


for model_name in model_names:
    print('\n')
    print('\n')
    print('############################################################################################################')
    print('\n')
    print('\n')
    print(model_name)
    # Loading the trained model
    model = mdl.load_model_(os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', model_name))
    model.summary()

    results = model.evaluate(test_images_generator, batch_size=param.BATCH_SIZE, verbose=1)
    del model

