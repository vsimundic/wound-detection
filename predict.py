import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from datamanipulation import preprocess as preprocess
from skimage.io import imread, imshow, imsave
from skimage.transform import resize

from dnnmodel import model as mdl
import parameters as param
import os

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

model = mdl.load_model_(os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', "model-wounds-zf_unet-val30.h5"))

for file_ in os.listdir(os.path.join(param.ROOT_PATH, "data", "test", "images")):
    print(file_)
    # load an image
    image = imread(os.path.join(param.ROOT_PATH, "data", "test", "images", file_))[:, :, :param.IMG_CHANNELS]
    image = resize(image, (param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), mode='constant',
                   preserve_range=True).astype(np.float32)
    image = image / 255.
    image = np.expand_dims(image, 0)

    mask_ = imread(os.path.join(param.ROOT_PATH, "data", "test", "masks", file_))

    mask_ = resize(mask_, (param.IMG_HEIGHT, param.IMG_WIDTH, 1), mode='constant', preserve_range=True)
    mask_ = mask_ / 255.
    mask_[mask_ >= 0.5] = 1.0
    mask_[mask_ < 0.5] = 0.0

    result = model.predict(x=image, verbose=1)
    result[result >= 0.5] = 1.0
    result[result < 0.5] = 0.0

    imname = file_[:-4]
    imname_mask = "".join([imname, '_mask.png'])
    imname = "".join([imname, '_predicted-zf_unet.png'])

    # print(imname)

    imsave(os.path.join(param.ROOT_PATH, "data", "test", "predictions", file_), image[0])
    imsave(os.path.join(param.ROOT_PATH, "data", "test", "predictions", imname), result[0])
    imsave(os.path.join(param.ROOT_PATH, "data", "test", "predictions", imname_mask), mask_)
    print("done")
