import os

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))

IMG_WIDTH = 320
IMG_HEIGHT = 320
IMG_CHANNELS = 3

TRAIN_PATH = os.path.join(ROOT_PATH, 'data', 'medetec-dataset-done')
TEST_PATH = os.path.join("/home/valentin/FAKS/MR/projekt/dataset/Project_MR/pics2_test")

LOAD_MODEL_PATH = os.path.join(ROOT_PATH, "data", "models", "model-wounds.h5")


SEED = 42
BATCH_SIZE = 2
EPOCHS = 500

val_split = 0.3
