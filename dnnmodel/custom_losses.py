from .custom_metrics import custom_mean_iou, dice_coef


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)


def mean_iou_loss(y_true, y_pred):
    return -custom_mean_iou(y_true, y_pred)
