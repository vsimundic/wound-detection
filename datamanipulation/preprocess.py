import os
from tqdm import tqdm
import parameters as param

import numpy as np
from skimage.io import imread
from skimage.transform import resize
from tensorflow.keras.preprocessing.image import ImageDataGenerator


np.random.seed(param.SEED)


def load_images():
    # iterating through images
    path_to_train_images, _, train_ids = next(os.walk(os.path.join(param.TRAIN_PATH, 'images')))
    path_to_train_masks, _, train_ids_masks = next(os.walk(os.path.join(param.TRAIN_PATH, 'masks')))

    val_split = param.val_split
    val_len = int(val_split * len(train_ids))

    np.random.shuffle(train_ids)

    val_ids = train_ids[-val_len:]
    train_ids = train_ids[:-val_len]

    train_ids_masks = train_ids
    val_ids_masks = val_ids

    ##
    # RESIZE TRAIN
    x_train = np.zeros((len(train_ids), param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), dtype=np.float32)
    y_train = np.zeros((len(train_ids), param.IMG_HEIGHT, param.IMG_WIDTH, 1), dtype=np.float32)

    for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
        img = imread(os.path.join(path_to_train_images, id_))[:, :, :param.IMG_CHANNELS].astype("float32")
        img = resize(img, (param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), mode='constant', preserve_range=True)
        img = img / 255.
        x_train[n] = img

    for n, id_mask_ in tqdm(enumerate(train_ids_masks), total=len(train_ids_masks)):
        try:
            mask_ = imread(os.path.join(path_to_train_masks, id_mask_)).astype("float32")
            mask_ = resize(mask_, (param.IMG_HEIGHT, param.IMG_WIDTH, 1), mode='constant', preserve_range=True)
            mask_ = mask_ / 255.
            mask_[mask_ >= 0.5] = 1.0
            mask_[mask_ < 0.5] = 0.0
            y_train[n] = mask_
        except FileNotFoundError as e:
            print(e)
            # x = e.filename
            continue

    # RESIZE VALIDATION
    x_val = np.zeros((len(val_ids), param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), dtype=np.float32)
    y_val = np.zeros((len(val_ids), param.IMG_HEIGHT, param.IMG_WIDTH, 1), dtype=np.float32)

    file = open(os.path.join(param.ROOT_PATH, 'data', 'validation_names.txt'), 'w')

    for n, id_ in tqdm(enumerate(val_ids), total=len(val_ids)):
        img = imread(os.path.join(path_to_train_images, id_))[:, :, :param.IMG_CHANNELS].astype("float32")
        img = resize(img, (param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), mode='constant', preserve_range=True)
        img = img / 255.

        x_val[n] = img

        file.write(os.path.join(path_to_train_images, id_) + '\n')

    file.close()

    for n, id_mask_ in tqdm(enumerate(val_ids_masks), total=len(val_ids_masks)):
        try:
            mask_ = imread(os.path.join(path_to_train_masks, id_mask_)).astype("float32")
            mask_ = resize(mask_, (param.IMG_HEIGHT, param.IMG_WIDTH, 1), mode='constant', preserve_range=True)
            mask_ = mask_ / 255.

            mask_[mask_ >= 0.5] = 1.0
            mask_[mask_ < 0.5] = 0.0

            y_val[n] = mask_
        except FileNotFoundError as e:
            print(e)

    return x_train, y_train, len(train_ids), x_val, y_val, len(val_ids)


def get_train_generator():
    x_train, y_train, _, x_val, y_val, _ = load_images()

    # Making data generators
    train_datagen = ImageDataGenerator(  # rescale=1./255,
        shear_range=0.3,
        zoom_range=0.3,
        horizontal_flip=True,
        # vertical_flip=True,
        rotation_range=40,
        fill_mode='nearest',
    )

    train_image_generator = train_datagen.flow(x_train, y_train,
                                               batch_size=param.BATCH_SIZE,
                                               shuffle=True, seed=param.SEED,
                                               # save_to_dir=os.path.join(param.ROOT_PATH, "data", "medetec-dataset-done", "augmented"),
                                               )

    val_datagen = ImageDataGenerator(  # rescale=1./255,
    )
    val_image_generator = val_datagen.flow(x_val, y_val, batch_size=param.BATCH_SIZE, shuffle=True, seed=param.SEED)

    return train_image_generator, val_image_generator
