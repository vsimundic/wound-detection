import os
import numpy as np
from skimage.io import imread
from skimage.transform import resize
from tqdm import tqdm
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import parameters as param


np.random.seed(param.SEED)


def load_test_images():
    # iterating through images and masks
    path_to_test_images, _, test_ids = next(os.walk(os.path.join(param.ROOT_PATH, 'data', 'test', 'images')))
    path_to_test_masks = os.path.join(param.ROOT_PATH, 'data', 'test', 'masks')

    x_test = np.zeros((len(test_ids), param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), dtype=np.float32)
    y_test = np.zeros((len(test_ids), param.IMG_HEIGHT, param.IMG_WIDTH, 1), dtype=np.float32)

    for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
        img = imread(os.path.join(path_to_test_images, id_))[:, :, :param.IMG_CHANNELS].astype("float32")
        img = resize(img, (param.IMG_HEIGHT, param.IMG_WIDTH, param.IMG_CHANNELS), mode='constant', preserve_range=True)
        img = img / 255.
        x_test[n] = img

        mask_ = imread(os.path.join(path_to_test_masks, id_)).astype("float32")
        mask_ = resize(mask_, (param.IMG_HEIGHT, param.IMG_WIDTH, 1), mode='constant', preserve_range=True)
        mask_ = mask_ / 255.
        mask_[mask_ >= 0.5] = 1.0
        mask_[mask_ < 0.5] = 0.0
        y_test[n] = mask_

    return x_test, y_test


def get_test_generator(x_test, y_test):

    test_images_generator = ImageDataGenerator()
    test_generator_data = test_images_generator.flow(x_test, y_test,
                                                     batch_size=param.BATCH_SIZE,
                                                     shuffle=True,
                                                     seed=param.SEED)
    return test_generator_data

