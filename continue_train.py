import tensorflow as tf

from datamanipulation import preprocess as preprocess
from dnnmodel import model as mdl
import parameters as param
import os

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as e:
        print(e)

train_images_generator, val_images_generator = preprocess.get_train_generator()

model = mdl.load_model_(os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', 'model-wounds-zf_unet.h5'))

res = mdl.continue_training(model, train_images_generator, val_images_generator, 1e-7)
