import tensorflow as tf
# import tensorflow.compat.v1 as tf
# tf.disable_v2_behavior()
# import keras
from datamanipulation import preprocess as preprocess
from dnnmodel import model as mdl
import parameters as param
import os

config = tf.compat.v1.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.compat.v1.InteractiveSession(config=config)
print(tf.__version__)
print(tf.test.is_gpu_available())

physical_devices = tf.config.experimental.list_physical_devices('GPU')
assert len(physical_devices) > 0, "Not enough GPU hardware devices available"
config = tf.config.experimental.set_memory_growth(physical_devices[0], True)

train_images_generator, val_images_generator = preprocess.get_train_generator()

# model = mdl.build_model_zf_unet()
model = mdl.build_model1()
# model = mdl.build_model2()


# model = mdl.build_vgg19_model()
# model = mdl.build_resnet34_model()
# model = mdl.build_resnet101_model()

model.save(os.path.join(param.ROOT_PATH, 'data', 'models', 'pretrained_models', 'model-wounds-resnet101-val30.h5'))


# model = mdl.load_model_(os.path.join(param.ROOT_PATH, 'data', 'models', 'saved_models', 'model-wounds-resnet101-val30.h5'))
# model = mdl.compile_model(model)

res = mdl.train_model_generator(model, train_images_generator, val_images_generator, 'model-wounds-resnet101-val30.h5')
